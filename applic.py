# coding: utf-8
"""
Simple Usage:
    $ python applic.py
"""
import stat
import zipfile
import json

#AUTH = {'Login': 'Konstantin.6or',
#        'Passwd': 'ggl_10293847'}
__AUTH = {'Login': 'kostin-g',
          'Passwd': 'r_10293847_y'}

__PROVS = {
        'yandex': ['smpt.yandex.ru','25','449'],
        'mail': ['mail.ru','25'],
        'google': ['google.com','25','449']
        }

service = 'gmail'
_AUTH = json.load(open('provs.json'))[service]
AUTH = {}
AUTH['Login'] = _AUTH[0]
AUTH['Passwd'] = _AUTH[1]

PROVS = json.load(open('prv.json'))
__author__ = '%s@%s (Konstantin Gorbunov)' % (AUTH['Login'], PROVS[service][0])

import sys, os
import datetime

sys.path.insert(0, '/usr/lib/python2.6/site-packages')
#print sys.path

import site #, audiodev, wave, dis
from email import encoders
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
import mimetypes

PostData = {'signIn': u'Войти',
            'PersistentCookie':'yes',
            'rmShown':'1'}

PostData.update(AUTH)
PostData.update({'Email': u'%s@gmail.com' % AUTH['Login']})

"""
sys.path.insert(0,r'd:\tools\Python27\Lib\site-packages')
sys.path.insert(0,r'd:\tools\Python27\Lib\site-packages\dbf')
sys.path.insert(0,r'd:\tools\Python27\Lib\site-packages\dbfpy')
sys.path.insert(0,r'd:\tools\Python27')
sys.path.insert(0,site.USER_SITE)
"""

#import antigravity

#print  site.USER_SITE #runpy.sys.getprofile()
#print  dis.HAVE_ARGUMENT #runpy.sys.getprofile()

HEAD = ['DOC_UNO','FILIAL','DOCUMENT_DATE','DOCUMENT_NUM','DOCUMENT_USER','ACC_DT','ACC_KT',
        'DATE_DOC','DATE_PROV','DATE_VAL','PRIORITET','SUM','SUM_NT',
        'VID_DOC','BUDGET_PAYMENTL','KL_DT_INTERNAL','KL_KT_INTERNAL','NAZN'
        ]

COL_WIDTH = [8,0,0,0,15,20,20,10,10,10,0,7,8,14,0,0,0,0]

LINK = {u'ОПОЩК2': u'Прохладный', u'ОПОЩК3': u'Нальчик', u'N ОО05': u'Майский',
        u'ООПО04': u'Чегем', u'ООПО06': u'Нарткала', u'ООПО07': u'Зольский', u'ООПО10': u'Баксан',
        }

EXTEND = [{'DOC_DATE': 10},
          {'DATA_PROV': 10},
    	  {'DATA_VAL': 10},
          {'PRIORITET':14},
          ]

MONTH = [u'Январь', u'Февраль', u'Март', u'Апрель', u'Май', u'Июнь', u'Июль', u'Август']

from zipfile import ZipFile as ZF

def _dbf(db_src):
    try:
        db = dbf.DBF(db_src)

        # Database processing
        #with db:
        #     print data
    except:
           print 'Error'
           return

def load_dbf(db_src):
    from dbf.db_exp import dbf2txt
    dbf2txt(db_src)
    """
    try:
        db = dbfpy.Dbf(db_src)
        db_cont = []
        for rec in db:
            _rec_ = ''
            for fld in rec:
                _rec_ += fld.encode('cp866')
            db_cont.append(_rec_)
        cont = open('test.txt', 'a+')
        cont.write('\n'.join(db_cont))
        cont.close()

    # Database processing
        #with db:
        #     print data
    except:
           print 'Error'
           return
    """

def load_xml(src_inf):
    xml_doc = m_dom.parse(src_inf)
    #xml_doc.normalize()
    nodes = xml_doc.documentElement.childNodes
    #print nodes
    for nd in nodes:
 	#if nd.nodeValue.strip():
	#	print nd.nodeValue.strip()
 	print nd.parentNode
 	print nd.childNodes
	print nd.getAttribute('name')
        #for ch in
    	if len(nd.childNodes):
		print dir(nd.childNodes[1]) #.parentNode.__dict__
		#print nd.childNodes[1].nodeName
		#print nd.childNodes[1].getAttribute('name')

def build_format():
    	    """ Формируем формат шаблона строк"""
	    HEAD_COLS = dict(zip(HEAD, COL_WIDTH))
    	    fmt = []
	    for cn in HEAD:
		wdth = HEAD_COLS[cn] or len(cn)
		#sys.stdout.write('%s' % wdth)
                fmt_s = '%s'
		if wdth:
		    wdth += 2
		    fmt_s = '%%%ds' % wdth
		    #sys.stdout.write('_%s\n' % wdth)
            	    #fmt.append('%%%ds' % wdth)
		#else:
            	fmt.append(fmt_s)
            	    #fmt_s = '%s'
	    return fmt

def load_txt(strg, pth=None):
    """
    " @ strg - список, содержащий строки для выгрузки в файл
    """
    reg = '003'
    #os.chdir(os.path.abspath + '/' + 
    if pth:
        os.chdir(pth) #[:pth.rindex('/')])
    # перебор списка файлов в каталоге
    #print os.path.abspath('.')
    for ech in os.listdir(os.curdir):
        if ech.endswith('V01') and not ech.startswith('doc_'):
            file_out = r'doc_%s_%s' % (reg, ech)
            if os.path.isfile(file_out):
                os.remove(file_out)
            # на случай, если имя файла не латинское
            f_n = ech #.decode('cp1251')
            print "=> %s" % f_n #.encode('utf-8')
            # если имя начинается с одного из ключей словаря, получаем префикс
            pref = ''
            #print LINK
	    for prf in LINK.keys():
 	        if f_n.startswith(prf.encode('cp1251')):
 	 	    pref = LINK[prf]
		    break

            # создание формата строк
	    fmt = build_format()
	    #print fmt
            _m = curr_time.strftime('%m')
            mnth = MONTH[int(_m)]
	    tm_val = cr_tm
            _y = curr_time.strftime('20%y')
	    sum1 = sum2 = '0.00'
            tkns_mask = ['', reg, cr_tm, '99', 'PENSIYA_DOC_%s' % reg, cr_tm, cr_tm, tm_val, '5', sum1, sum2, u'БЕЗН_БАНК_ОРД', 'false', 'true', 'true']

            #print '%s' % f_n.encode('utf-8')
	    #print tkns_mask
            #print os.path.abspath('.')
            with open(f_n) as rec:
                    tkns = rec.readline().split()
		    if len(tkns) and tkns[0].isdigit():
			tkns_tmp = map(lambda x: x.decode('cp866'), tkns)
			tkns_mask[5:5] = [tkns_tmp[-3]] *2
			tkns = tkns_mask + tkns_tmp[7:-3]
			tkns.append(u'%sЗачисление пенсии за %s %s г. %s' % (pref, mnth, _y, ' '.join(tkns[22:25])))

                    # добавление заголовка
            	    #strg = reduce(lambda x: '<%%%ds>' % x, zip(fmt, HEAD))
		    #print strg

		    # добавление строки
            	    strg.append(''.join(fmt) % tuple(tkns))
	    #for ln in strg:
 	    #	print '>%s<' % ln
            out2file = '\n'.join(strg).encode('utf-8')
            exp2file(file_out, out2file)

def exp2file(file_out, out2file):
    try:
        fo = open(file_out, 'w')
        fo.write(out2file)
        fo.close()
    except:
  	    print u'Ошибка записи в файл'

def send_rep():
    print u'Report sent.'

def get_lst(pth):
    # Формируем список файлов и каталогов
    os.chdir(pth)
    lst = {'f': [], 'd': []}
    c_p = 'cp1251'
    for l in os.listdir('.'):
        if os.path.isfile(l):
            lst['f'].append(l.decode(c_p))
        elif os.path.isdir(l):
            lst['d'].append(l.decode(c_p))
    return lst

def to_arh(arh, pth):
    ''' Создаем архив, в который упакованы файлы из списка
    @arh - имя архива
    @zf - архив
    @lst - файлы
    '''
    lst = get_lst(pth)['f']
    zf = ZF(arh + '.zip', 'w')
    map(lambda x: zf.write(x, x.encode('cp866')),
        [f for f in lst if f.endswith('01')])
    zf.close()

marks = [['123','312','134','234'],['213','312','143','234']]
res = [] # * len(marks[0])
full_res = []

def chk_res(m1, m2):
    #print res
    def chk(ms1,ms2):
        #if len(m1) > 1:
        if ms1 != ms2:
            res.append(1)

    map(chk, m1, m2)
    full_res.append(sum(res))

class Pochta(object):
    def __init__(self, srv=None, port=25):
        if srv:
            self.srv = srv
            self.port = port

        #import MIMEMultipart
        import smtplib
        #import imaplib
        self.pismo_out = smtplib.SMTP() #mail_msk % 'aspmx.l'.replace('@','.'))
        #self.pismo_in = imaplib.IMAP4()
            #if not self.pismo:
            #    self.pismo.

    def send(self, content):
        #print content
        otpr = poluch = MAIL_MSK[1] % (AUTH['Login'] + '@') # content['From']
        self.infa = MIMEMultipart()
        #self.infa.attach(MIMEText(content['Message'], 'plain'))
        self.infa['From'] = otpr
        self.infa['To'] = poluch
        self.infa['Subject'] = content['Subject']
        self.infa.attach(MIMEText(inf['Message'], 'plain'))
        #self.infa.attach() #content['Message'] % self.infa

        if self.infa:
            #self.attach(content['List']) #self.ttl)
            #self.infa.attach(self.part) #inf['Message'])
            #self.attach(MIMEText(inf['Message'], 'plain'))
            if content['List']:
               self.attach(content['List'])

            self.pismo.connect(self.srv, self.port)
            self.pismo.login(AUTH['Login'], AUTH['Passwd'])
            self.pismo.sendmail(otpr, poluch, self.infa.as_string())
            self.pismo.quit()

    def attach(self, lst):
        #lst = ['603.Q01','803.Q01'] #[u'ООПО0603.V01']
        #import mimetypes
        #try:
        #   ctype, encoding = mimetypes.guess_type(PTH)
        #except:
            #if ctype is None or encoding is not None:
        ctype = 'application/octet-stream'
        maintype, subtype = ctype.split('/', 1)
        for l in lst:
            if maintype == 'text':
                fp = open(l)
                #self.part = MIMEText(fp.read(), _subtype=subtype)
                fp.close()
            else:
                fp = open(l.decode('utf-8'), 'rb')
                self.part = MIMEBase(maintype, subtype)
                self.part.add_header('Content-Disposition', 'attachment', filename=l)
                self.part.set_payload(fp.read())
                fp.close()
                # Encode the payload using Base64
                encoders.encode_base64(self.part)
                self.infa.attach(self.part)

def show_web():
    import urllib2, webbrowser
    webbrowser.open_new_tab('index.html')
    #os.system('index.html')

def empt_dirs(pth):
    html_msk = ''
    for ctl in get_lst(pth)['d']:
        sub_lst = os.listdir(ctl)
        empty = []
        if sub_lst:
            #print ctl
            def chk_file(elm):
                try:
                    print u'Размер файла: %d\n' % os.stat(elm).st_size
                except:
                    print u'%s, Ошибка' % elm
            map(chk_file, sub_lst)
            #print u'Файлы: \n' + '\n'.join(sub_lst)
        else:
            empty.append(ctl)
            os.rmdir(ctl)
    if empty:
        html_res = u'Слудующие каталоги удалены (пустые):\n' + \
                   '\n'.join(empty.append('---------------'))
    else:
        html_res = 'Result'

    #obj = os.stat(ctl)
    #print obj.getsize(ctl)
    #print obj
    #print stat.IS_DIR(obj.st_mode)

def create_serv():
    import socket
    sckobj = socket(AF_INET, SOCK_STREAM)
    sckobj.bind('localhost','')
    sckobj.listen(5)
    while True:
        conn, adr = sckobj.accept()
        print "On %s" % adr
        while True:
            data = conn.recv(1024)
            if not data: break
            conn.send(b'Echo =>%s' % data)
        conn.close()

if __name__ == "__main__":
    curr_time = datetime.date.today()
    cr_tm = curr_time.strftime('%d/%m/%Y')
    dt_tm = curr_time.strftime('%Y_%m_%d')

    PTH = r'd:\doki\vipnet'
    ext = {u'Нальчик': ['?01', 'txt'],
           u'Москва': ['??']}

    MAIL_MSK = ['%sgmail.com', '%syandex.ru']
    inf = {'Subject': 'Отправка почты',
           'Title': 'From: %(From)s\r\nTo: %(To)s\r\n'+\
                    'Content-Type: text/html; charset="utf-8"\r\n'+\
                    'Subject: %(Subject)s\r\n\r\n',
           'Message': 'Проверяю отправку с вложением',
           'List': ['ООПО0603.V01', '203.Q01', 'doc_0_ООПО0403.V01']}

    # Архивация
    #to_arh(dt_tm, PTH)

    """
    # Проверка пустых каталогов
    import user
    u_h = user.home
    #tmp_browse = r'c:\Documents and Settings\k07ter
    tmp_browse = r'%s\Local Settings\Application Data\Opera\Opera\cache' % u_h
    empt_dirs(tmp_browse)
    """

    # Отправка почты
    srv = MAIL_MSK[1] % 'smtp.'
    ##mylo = Pochta(srv)
    #mylo.send(inf)
    #

    #import win32con
    #show_web()
    #from dbf import *
    #import httplib2, urllib2
    #from processes.urlfetch2 import UrlFetch
    #import SocketServer, statvfs
    #print dir(SocketServer)
    #print statvfs.F_FILES
    #ftch = UrlFetch()
    #ftch.io['input'].put((ClientLoginUrl, 'POST', ClientLoginParams))
    #ftch.run_once()

    """
    import requests
    sess = requests.Sessions()
    req = sess.post("https://www.accounts.google.com/ServiceLogin", PostData)
    print req
    #from pyquery import PyQuery

    from apiclient import sample_tools as st, errors

    #print dir(st.client)
    #print st.client.__dict__ #OAuth2WebServerFlow.__dict__
    #print st.client.OAuth2WebServerFlow.step1_get_authorize_url.__dict__
    #st.client.OAuth2WebServerFlow()
    #st.client.OAuth2WebServerFlow.step1_get_authorize_url()

    #print dir(st.client.clientsecrets) #.GOOGLE_AUTH_URI
    #print st.client.flow_from_clientsecrets() #.GOOGLE_AUTH_URI
    #auth = st.client.AccessTokenCredentials('konstantin.6or@gmail.com','ggl_10293847')
    #auth.authorize()
    #print dir(auth)
    #print auth.store.__dict__

    from oauth2client.file import Storage
    from oauth2client.client import AccessTokenRefreshError, flow_from_clientsecrets
    from oauth2client.tools import run
    #print dir(oauth2client.client)
    CLIENT_SECRETS = 'client_secrets.json'
    FLOW = flow_from_clientsecrets(CLIENT_SECRETS,
    scope='https://www.googleapis.com/auth/analytics.readonly',
    message='MISSING_CLIENT_SECRETS_MESSAGE')

    strg = Storage('sampl.dat')
    cred = strg.get()
    if cred:
        cred_t = run(FLOW, strg)
    else:
        cred_t = ''
    print cred_t

    from apiclient.discovery import build
    url = 'http://ajax.googleapis.com/ajax/services/feed/load?v=1.0&num=1&q='
    link = 'http://www.opennet.ru/opennews/opennews_all_noadv.rss'
    request = urllib2.Request(url+urllib2.quote(link, safe='~()*!.\''), None)
    response = urllib2.urlopen(request)
    #results = simplejson.load(response)
    print response.code
    """
    #print dir(response.readlines()[0])
    #print rsp

    '''
    print apiclient.model.__dict__.keys()
    print apiclient.model.JsonModel.__dict__.keys()
    print apiclient.model.BaseModel.__dict__.keys()
    print apiclient.http.simplejson.__dict__.keys()
    print apiclient.http.simplejson.load.__dict__
    print apiclient.http.simplejson.loads
    '''

    """
    import subprocess
    player = r'c:\Program Files\MPC-HC.1.6.4.6052.x86\mpc-hc.exe'
    f=['opr017P9','opr01BNO']
    plr = subprocess.Popen(r'd:\vid\%s.tmp' % f[1], executable=player, shell=False, stdout=subprocess.PIPE,
                           stdin=subprocess.PIPE, stderr=subprocess.PIPE, env=None)
    import poplib, imaplib, sshlib, telnetlib, httplib
    """
    import spc_dbf.db_exp as sd
    #_ctl_ = 'spc_dbf'
    #sd.dbf2txt('%s/0613100.DBF' % _ctl_)

    #import subprocess, pickle, email, chunk
    #a_ch = asynchat.async_chat
    #a_ = a_ch().connect('localhost')

    load_txt([], 't01')
    print 'Loaded'
    #print stat('new').S_IFDIR
    #aa = ['qw','df','yt','reg']
    #print map(None, ['%s'] * 5, aa)
    #while True:
 	#conn.modules.
	#dr = os.stat('new')
	#print dir(dr)
	#print dr.st_atime
	#print dr.st_ctime
    #curr_time = datetime.date.today()
	#sys.stdout.write('step\n')
